﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;

public class SpriteEditor : EditorWindow {
    private Texture2D texture;
    private Color color;
    private int width;
    private int height;
    private SpritePixel[,] pixels;
    public static SpriteEditor editor;
    public bool changed;
    [MenuItem("Window/SpriteEditor")]
    static void Init()
    {
        // Get existing open window or if none, make a new one:
        editor = EditorWindow.GetWindow<SpriteEditor>();
        editor.Show();
    }


    // Update is called once per frame
    void OnGUI () {
        if (editor == null) editor = this;
        int w = EditorGUILayout.IntField(width);
        int h = EditorGUILayout.IntField(height);
        if(h != height || w != width)
        {
            height = h;
            width = w;
            texture = new Texture2D(width, height);
            Generate();
        }
        if (GUILayout.Button("Create Texture"))
        {
            texture = new Texture2D(width, height);
        }
        if (GUILayout.Button("SaveTexture"))
        {

            Apply();
            File.WriteAllBytes(EditorUtility.SaveFilePanel("Save texture", "", "texture", "png"), texture.EncodeToPNG());
        }
        Texture2D target = (Texture2D)EditorGUILayout.ObjectField("Image", texture, typeof(Texture2D), false);
        color = EditorGUILayout.ColorField(color);
        if(target != texture)
        {
            texture = target;
            Generate();
        }
        if (texture == null) return;
        if (pixels == null)
        {
            Generate();
            changed = true;
        }
        Redraw();
        
	}
    public void Apply()
    {
        for (int x = 0; x <= pixels.GetUpperBound(0); x++)
        {
            for (int y = 0; y <= pixels.GetUpperBound(1); y++)
            {
                texture.SetPixel(x, y, pixels[x, y].color);
            }
        }
        texture.Apply();
    }
    public void Redraw()
    {
        for (int x = 0; x < pixels.GetUpperBound(0); x++)
        {
            for (int y = 0; y < pixels.GetUpperBound(1); y++)
            {
                pixels[x, y].Draw();
                pixels[x, y].HandleEvents(Event.current, color);
            }
        }
        changed = false;
    }
    public void Generate()
    {
        pixels = new SpritePixel[texture.width, texture.height];
        for (int x = 0; x < texture.width; x++)
        {
            for (int y = 0; y < texture.height; y++)
            {
                pixels[x, y] = new SpritePixel(texture.GetPixel(x, y), x, y);
            }
        }
        changed = true;


    }
}
