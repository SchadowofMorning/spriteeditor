﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class SpritePixel
{
    public Color color;
    public int x;
    public int y;
    private GUIStyle style;
    private Rect rect;
    public SpritePixel(Color cl, int _x, int _y)
    {
        color = cl;
        x = _x;
        y = _y;
        style = new GUIStyle(GUI.skin.box);
        style.normal.background = MakeTex(2, 2, color);
        rect = new Rect(x * 10 + 50, y * 10 + 200, 10, 10);
    }
    public void Draw()
    {
        GUI.Box(rect, new GUIContent(), style);
    }
    public void HandleEvents(Event e, Color clr)
    {
        switch (e.type)
        {
            case EventType.MouseDown:
                if (rect.Contains(e.mousePosition))
                {
                    color = clr;
                    style.normal.background = MakeTex(2, 2, color);
                    SpriteEditor.editor.changed = true;
                }
                break;
            case EventType.MouseDrag:
                if (rect.Contains(e.mousePosition))
                {
                    color = clr;
                    style.normal.background = MakeTex(2, 2, color);
                    SpriteEditor.editor.changed = true;
                    e.Use();
                }
                break;
        }
    }
    private Texture2D MakeTex(int width, int height, Color col)
    {
        Color[] pix = new Color[width * height];
        for (int i = 0; i < pix.Length; ++i)
        {
            pix[i] = col;
        }
        Texture2D result = new Texture2D(width, height);
        result.SetPixels(pix);
        result.Apply();
        return result;
    }
}